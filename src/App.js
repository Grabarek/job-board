import React from 'react';
import { Route } from 'react-router-dom'

// Components
import Nav from './components/Nav';
import Offers from './components/Offers';
import News from './components/News';
import Brands from './components/Brands';
import Login from './components/Login';
import PostJob from './components/PostJob';


// Styles
import './scss/App.scss';

function App() {
  return (
    <div className="App">
      <Nav />

      <Route exact path="/" component={Offers} />
      <Route exact path="/news" component={News} />
      <Route exact path="/brands" component={Brands} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/postjob" component={PostJob} />

    </div>
  );
}

export default App;
