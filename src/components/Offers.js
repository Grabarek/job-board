import React from 'react';

import Filters from './Filters';
import Map from './Map';
import OffersList from './OffersList';
import ActiveOffer from './ActiveOffer';

class Offers extends React.Component {

    state = {

    }

    render() {

        return (
            <React.Fragment>
                {/*Filters*/}
                <Filters/>

                <div className="offers">
                    <div className="container">
                        <div className="offers__wrapper">
                            <div className="offers__content">
                                {/* Offers list */}
                                <OffersList/>
                                {/* Single offer */}
                                <ActiveOffer/>
                            </div>
                            {/* Map */}
                            <Map/>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }


}

export default Offers;
