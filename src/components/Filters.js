import React from 'react';

class Filters extends React.Component {

    state = {

    }

    render() {
        const {cities} = this.state;

        return (
            <div className="filters">
                <div className="container">
                    <div className="filters__wrapper">
                       <p className="filter active">
                           All
                       </p>
                        <p className="filter">
                            Warszawa
                        </p>
                        <p className="filter">
                            Łódź
                        </p>
                    </div>
                </div>
            </div>
        )
    }


}

export default Filters;
