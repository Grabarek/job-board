import React from 'react';
import { NavLink } from "react-router-dom";
class Nav extends React.Component {

    state = {

    }

    render() {

        return (
            <nav className="nav">
                <div className="container">
                    <div className="nav__wrapper">
                        <div className="nav__left">
                            <NavLink activeClassName="active" exact to="/">
                                <img src="react.svg" alt="logo" className="nav__logo" />
                            </NavLink>
                            <ul className="menu__items">
                                <li className="menu__item">
                                    <NavLink activeClassName="active" exact to="/">
                                        Job offers
                                    </NavLink>
                                </li>
                                <li className="menu__item">
                                    <NavLink activeClassName="active" exact to="/brands">
                                        Brands
                                    </NavLink>
                                </li>
                                <li className="menu__item">
                                    <NavLink activeClassName="active" exact to="/news">
                                        News
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                        <div className="nav__right">
                            <NavLink activeClassName="active" className="postjob" to="/postjob">
                                Post a Job
                            </NavLink>
                            <NavLink activeClassName="active" className="login" to="/login">
                                Sign in
                            </NavLink>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }


}

export default Nav;
